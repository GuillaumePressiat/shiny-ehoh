
### Description
Outil interactif d'exploration et de diffusion des listes de diagnostics CIM-10 des manuels de GHM.



------
### Utilisation


#### Onglet Version de la CIM-10

Choisir l'année.

#### Onglet Cim-10

L'utilisateur dispose de la CIM-10 en vigueur pour l'année sélectionnée et peut filtrer sur les colonnes ou rechercher par mot clé.

#### Onglet Listes manuel GHM

Rechercher une liste dans les listes du manuel des GHM, par libellé ou par numéro.

#### Onglet Générer une liste

à partir des numéros de listes du manuel des GHM, en copiant un numéro, la liste des codes est affichée. 


#### Appartenance

Le diagnostic copié dans le formulaire appartient-il à une ou plusieurs listes ? Les listes sont affichées.

