
### Description
Outil interactif d'exploration et de diffusion des listes d'actes CCAM des manuels de GHM.



------
### Utilisation


#### Onglet Version de groupage

Choisir l'année.

#### Onglet hiérarchie

L'utilisateur dispose de la hiérarchie  et peut filtrer sur les colonnes ou rechercher par mot clé.

#### Onglet Ccam

L'utilisateur dispose des actes CCAM et peut filtrer sur les colonnes ou rechercher par mot clé.

#### Onglet Listes Manuel GHM

Rechercher une liste dans les listes du manuel des GHM, par libellé ou par numéro.

#### Onglet Générer une liste

à partir des numéros de listes du manuel des GHM, en copiant un numéro, la liste des codes est affichée. 


#### Appartenance

L'acte copié dans le formulaire appartient-il à une ou plusieurs listes ? Les listes sont affichées.

