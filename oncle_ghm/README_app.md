
### Description
Regroupements GHM / RGM & tarifs GHS + suppléments


------
### Utilisation


#### Onglet Version de la classification

Choisir l'année.


#### Onglet GHM ?

Obtenir les informations d'un GHM

#### Onglet RGP-GHM

Explorer et / ou exporter la table de regroupement ATIH des GHM

#### Onglet RGP-RGHM

Explorer et / ou exporter la table de regroupement ATIH des racines de GHM.

#### GHS

Explorer et / ou exporter la table des tarifs et des libellés de GHS pour les établissements publics

#### Suppléments

Explorer et / ou exporter les tarifs des suppléments pour les établissements publics

